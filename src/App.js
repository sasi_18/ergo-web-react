import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import LandingPage from '../src/Screens/LandingPage/index';
import Colors from './Utils/theme/Colors';

const theme = createMuiTheme({
  palette: {
    Colors
  },
});
function App() {
  return (
    <ThemeProvider theme={theme}>
      <LandingPage />
    </ThemeProvider>
  );
}

export default App;
