import React from "react";
import styled from "styled-components";
import Button from "@material-ui/core/Button";
//import {useTheme} from '@material-ui/core';
// const theme = useTheme();

export const ButtonDefault = styled(Button)`
  border-radius: 3px;
  height: 36px;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  padding: 8px 12px;
  text-transform: none;
  background-color: #58c3b9;
`;

const ButtonSecondary = styled(ButtonDefault)`
  border: 1px solid ${(props) => props.theme.palette.Colors.neutral60};
`;

export default function DefaultButton(props) {
  const { variant, icon, label, children } = props;
  //console.log(props);
  const propsCopy = Object.assign({}, props);
  if (icon) {
    propsCopy.startIcon = <img src={icon} alt='icon' />;
  }

  switch (variant) {
    case "primary":
      return (
        <ButtonDefault
          data-testid='button'
          {...propsCopy}
          color='primary'
          variant='contained'
          disableElevation
        >
          {label || children}
        </ButtonDefault>
      );
    case "secondary":
      return (
        <ButtonSecondary
          data-testid='button'
          {...propsCopy}
          variant='outlined'
          color='primary'
        >
          {label || children}
        </ButtonSecondary>
      );
    case "disabled":
      return (
        <Button disabled variant='contained'>
          disabled
        </Button>
      );

    default:
      return (
        <ButtonDefault
          data-testid='button'
          {...propsCopy}
          color='primary'
          variant='contained'
        >
          {label || children}
        </ButtonDefault>
      );
  }
}

// Sample json
// property icon: should be remote url in dynamic/json
const button = {
  variant: "primary",
  label: "Submit",
  disabled: true,
  icon: "https://img.icons8.com/material-outlined/24/000000/cloud-network.png",
};
