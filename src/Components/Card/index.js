import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import Button from "../Button/index";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    // boxShadow: `0px 0px 9px ${theme.palette.KenColors.shadowColor}`,
    borderRadius: 3,
    [theme.breakpoints.up("md")]: {
      padding: 16,
    },
    [theme.breakpoints.down("sm")]: {
      padding: 8,
    },
    marginTop: 10,
    [theme.breakpoints.down("sm")]: {
      padding: 12,
    },
  },
  title: {
    textAlign: "start",
    fontSize: 18,
    fontWeight: "bold",
  },
  cardWrap: {
    justifyContent: "space-between",
  },
}));

export default function KenCard(props) {
  const { title, buttonLabel } = props;

  const classes = useStyles();

  return (
    <React.Fragment>
      {/* <Typography className={classes.mainTitle}></Typography> */}
      <Paper elevation={0} className={classes.root}>
        <Grid container item className={classes.cardWrap}>
          {title && (
            <Typography className={classes.title}>{title}</Typography>
          )}
          {buttonLabel && <Button>{buttonLabel}</Button>}
        </Grid>
        {props.children}
      </Paper>
    </React.Fragment>
  );
}
