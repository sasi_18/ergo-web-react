import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import CssBaseline from "@material-ui/core/CssBaseline";
import { Grid, makeStyles } from "@material-ui/core";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Link from "@material-ui/core/Link";

const useStyles = makeStyles((theme) => ({
  header: {
    background: "#fff",
  },
  wrapper: {
    width: "80%",
    margin: "0 auto",
    // backgroundColor: theme.palette.background.paper,
  },
  root: {
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
  linkItem: {
    color: "black",
    // paddingLeft: 40,
    padding: 10,
    borderRadius: 3,
    textAlign: "center",
    "&:hover": {
      background: "#58c3b9",
      color: "#fff",
    },
  },
}));

export default function ElevateAppBar(props) {
  const { title, label } = props;
  const classes = useStyles();
  const preventDefault = (event) => event.preventDefault();
  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar data-testid='header' className={classes.header}>
        <Toolbar>
          <div className={classes.wrapper}>
            <Typography className={classes.root}>
              <Link
                href='#'
                onClick={preventDefault}
                className={classes.linkItem}
              >
                Home
              </Link>
              <Link
                href='#'
                onClick={preventDefault}
                className={classes.linkItem}
              >
                Accounts
              </Link>
              <Link
                href='#'
                onClick={preventDefault}
                className={classes.linkItem}
              >
                Property Functions
              </Link>
              <Link
                href='#'
                onClick={preventDefault}
                className={classes.linkItem}
              >
                User Functions
              </Link>
              <Link
                href='#'
                onClick={preventDefault}
                className={classes.linkItem}
              >
                User Bookings
              </Link>
            </Typography>
          </div>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </React.Fragment>
  );
}
