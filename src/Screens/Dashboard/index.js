import React from "react";
import { Box, makeStyles, Grid, Typography } from "@material-ui/core";
import Card from "../../Components/Card/index";
import Table from "../../Components/Table/index";

const useStyles = makeStyles((theme) => ({
  root: {
    background: "lightgray",
    height: "100vh",
  },
  wrapper: {
    width: "80%",
    margin: "0 auto",
  },
  cardWrap: {
    marginTop: 30,
  },
  tableWrap: {
    marginTop: 30,
  },
}));

export default function Index(props) {
  const classes = useStyles();

  return (
    <Box className={classes.root}>
      <Box className={classes.wrapper}>
        <Grid className={classes.cardWrap}>
          <Card title='Users' buttonLabel='Create'></Card>
        </Grid>
        <Grid className={classes.tableWrap}>
          <Table />
        </Grid>
      </Box>
    </Box>
  );
}
