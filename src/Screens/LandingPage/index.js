import React from "react";
import { Box, makeStyles } from "@material-ui/core";
import Header from "../../Components/Header/index";
import MainLayout from "../Mainlayout/index";

const useStyles = makeStyles((theme) => ({
  root: {},
}));

function Index() {
  const classes = useStyles();
  return (
    <Box className={classes.root}>
      <Header />
      <MainLayout />
    </Box>
  );
}

export default Index;
