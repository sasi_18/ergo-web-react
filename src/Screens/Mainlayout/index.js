import React from "react";
// for routing
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import DashBoard from "../Dashboard/index";

function Index() {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={DashBoard} />
      </Switch>
    </Router>
  );
}

export default Index;
