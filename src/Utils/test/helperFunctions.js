import React from "react";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import Colors from "../theme/kenColors";

const theme = createMuiTheme({
	palette: {
		Colors,
	},
});

const provideTheme = (props) => {
	return <MuiThemeProvider theme={theme}>{props.children}</MuiThemeProvider>;
};

export default provideTheme;
